# Created by mdnssknght for 5.9
export PATH="$HOME/.cargo/bin:$PATH"

setopt prompt_subst

autoload -Uz vcs_info
precmd() { vcs_info }
zstyle ':vcs_info:*' formats ' %s(%F{yellow}%b%f)'

PS1='%~${vcs_info_msg_0_} $ '
