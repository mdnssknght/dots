## Information
- OS: [Artix Linux](https://artixlinux.org/) with [Runit](https://wiki.artixlinux.org/Main/Runit) init
- Window Manager: [Hyprland](https://hyprland.org/)
- Terminal: [Kitty](https://sw.kovidgoyal.net/kitty/)
- Font: [ShureTechMono Nerd Font](https://www.programmingfonts.org/#share-tech)
- Colorscheme: [Base16 Black Metal](https://github.com/metalelf0/base16-black-metal-scheme)
- Laptop: Huawei Matebook D14

## Screenshots
![Screenshot](assets/20231017_11h11m16s_grim.png)
![Screenshot](assets/20231017_11h11m32s_grim.png)
![Screenshot](assets/20231017_11h11m54s_grim.png)
